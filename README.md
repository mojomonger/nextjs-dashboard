## Next.js App Router Course - Starter

This is the starter template for the Next.js App Router Course. It contains the starting code for the dashboard application.

For more information, see the [course curriculum](https://nextjs.org/learn) on the Next.js Website.

Set up to use port 3066, set in package.json : "next dev -p 3066" entry

to start server, do: `npm run dev`
